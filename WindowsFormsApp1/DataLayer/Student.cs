﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DataLayer
{
    public class Student
    {
        public Student()
        {
            FirstName = null;
            LastName = null;
            StuNumber = null;
            Major = null;
            PhotoPath = null;
            State = false;
        }

        public Student(string FirstName, string LastName, string StuNumber, string Major, string PhotoPath, bool State)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.StuNumber = StuNumber;
            this.Major = Major;
            this.PhotoPath = PhotoPath;
            this.State = State;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StuNumber { get; set; }
        public string Major { get; set; }
        public string PhotoPath { get; set; }
        public bool State { get; set; }
    }
}
