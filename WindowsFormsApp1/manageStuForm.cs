﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DataLayer;

namespace WindowsFormsApp1
{
    public partial class manageStuForm : Form
    {
        DataBase db;
        public manageStuForm()
        {
            InitializeComponent();
            db = new DataBase();
            btnStuEdit.Enabled = false;
            btnStuDelete.Enabled = false;
            imgStuEdit.Enabled = false;
        }

        private void SelectedImg_Click(object sender, EventArgs e)
        {



            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "jpg files(*.jpg)|*.jpg| png files(*.png)|*.png|All Files(*.*)|*.*"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SelectedImg.ImageLocation = dialog.FileName;
            }
        }

        private void inputStuNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
                e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
                e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void stuNumberSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
                e.Handled = e.KeyChar != (char)Keys.Back;
        }

        private void btnAddNewStu_Click(object sender, EventArgs e)
        {
            if (inputStuFname.Text == "")
            {
                inputStuFname.Text = "نام را وارد کنید";
            }
            else if (inputStuLname.Text == "")
            {
                inputStuLname.Text = "نام خانوادگی را وارد کنید";
            }
            else if (inputStuNumber.Text == "")
            {
                inputStuNumber.Text = "شماره دانشجویی را وارد کنید";
            }
            else if (inputStuMajor.Text == "")
            {
                inputStuMajor.Text = "رشته را وارد کنید";
            }
            else
            {

                string FirstName = inputStuFname.Text;
                string LastName = inputStuLname.Text;
                string StuNumber = inputStuNumber.Text;
                string Major = inputStuMajor.Text;
                if (db.GetStudent(StuNumber) != null)
                {
                    MessageBox.Show("شماره دانشجویی صحیح نیست", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                var ext = Path.GetExtension(SelectedImg.ImageLocation);
                string path = Path.Combine(@"Image\", StuNumber + ext);
                if (File.Exists(Path.Combine(Environment.CurrentDirectory, path)))
                {
                    File.Delete(Path.Combine(Environment.CurrentDirectory, path));
                }
                File.Copy(SelectedImg.ImageLocation, Path.Combine(Environment.CurrentDirectory, path));
                db.InsertStudent(new Student(FirstName, LastName, StuNumber, Major, path, false));

                MessageBox.Show("دانشجو با موفقیت ثبت شد", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                inputStuFname.Text = "";
                inputStuLname.Text = "";
                inputStuNumber.Text = "";
                inputStuMajor.Text = "";
                SelectedImg.ImageLocation = null;
            }
        }

        private void btnStuNumberSearch_Click(object sender, EventArgs e)
        {
            string stuNumber = stuNumberSearch.Text;
            if (stuNumber == "")
            {
                MessageBox.Show("شماره دانشجویی را وارد کنید", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var student = db.GetStudent(stuNumber);
            if (student == null)
            {
                MessageBox.Show("شماره دانشجویی صحیح نیست", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            editStuFname.Text = student.FirstName;
            editStuLname.Text = student.LastName;
            editStuNumber.Text = student.StuNumber;
            editStuMajor.Text = student.Major;
            imgStuEdit.ImageLocation = Path.Combine(Environment.CurrentDirectory, student.PhotoPath);
            btnStuEdit.Enabled = true;
            btnStuDelete.Enabled = true;
            imgStuEdit.Enabled = true;
        }

        private void btnStuDelete_Click(object sender, EventArgs e)
        {
            db.DeleteStudent(editStuNumber.Text);
            MessageBox.Show("دانشجو با موفقیت حذف شد", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            editStuFname.Text = "";
            editStuLname.Text = "";
            editStuNumber.Text = "";
            editStuMajor.Text = "";
            imgStuEdit.ImageLocation = "";
            btnStuEdit.Enabled = false;
            btnStuDelete.Enabled = false;
            imgStuEdit.Enabled = false;
            stuNumberSearch.Text = "";
        }

        private void imgStuEdit_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "jpg files(*.jpg)|*.jpg| png files(*.png)|*.png|All Files(*.*)|*.*"
            };

            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                imgStuEdit.ImageLocation = dialog.FileName;
            }
        }

        private void btnStuEdit_Click(object sender, EventArgs e)
        {
            var student = new Student();
            student.FirstName = editStuFname.Text;
            student.LastName = editStuLname.Text;
            student.StuNumber = editStuNumber.Text;
            student.Major = editStuMajor.Text;
            var ext = Path.GetExtension(imgStuEdit.ImageLocation);
            string path = Path.Combine(@"Image\", editStuNumber.Text + ext);
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, path)))
            {
                File.Delete(Path.Combine(Environment.CurrentDirectory, path));
            }
            File.Copy(imgStuEdit.ImageLocation, Path.Combine(Environment.CurrentDirectory, path));
            student.PhotoPath = path;
            db.EditStudent(student, stuNumberSearch.Text);
            MessageBox.Show("دانشجو با موفقیت ویرایش شد", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            editStuFname.Text = "";
            editStuLname.Text = "";
            editStuNumber.Text = "";
            editStuMajor.Text = "";
            imgStuEdit.ImageLocation = "";
            btnStuEdit.Enabled = false;
            btnStuDelete.Enabled = false;
            imgStuEdit.Enabled = false;
            stuNumberSearch.Text = "";
        }
    }
}
